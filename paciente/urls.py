from django.contrib import admin
from django.urls import path
from .models import Paciente
from .views import PacienteListView, PacienteCreateView, PacienteUpdateView, PacienteDeleteView

urlpatterns = [
    path('paciente', PacienteListView.as_view(), name='listar_paciente'),
    path('ingresar_paciente', PacienteCreateView. as_view(), name='ingresar_paciente'),
    path('editar_paciente/<int:pk>', PacienteUpdateView.as_view(), name='editar_paciente'),
    path('eliminar_paciente/<int:pk>', PacienteDeleteView.as_view(), name='eliminar_paciente'),
]

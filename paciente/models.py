from django.db import models
from .constans import TIPDOC
from datetime import datetime
from django.conf import settings
from dateutil.relativedelta import relativedelta
from django.core.validators import MinLengthValidator


class Paciente(models.Model):
    tipo_documento = models.CharField('TIPO DE DOCUMENTO', max_length=1, choices=TIPDOC, blank=False, null=False, default=1)
    numero_documento = models.CharField('NUMERO DE DOCUMENTO', max_length=15, validators=[
                                        MinLengthValidator(8)], blank=False, null=True)
    nombres = models.CharField('NOMBRES', max_length=120, blank=True, null=True)
    apellido_paterno = models.CharField('APELLIDO PATERNO', max_length=90, blank=True, null=True)
    apellido_materno=models.CharField('APELLIDO MATERNO', max_length=90, blank=True, null=True)
    fecha_nacimiento = models.DateField('FECHA DE NACIMIENTO', blank=False, null=True)

    class Meta:
        unique_together = ('tipo_documento', 'numero_documento')

    def __str__(self):
        return self.nombre_completo

    @property
    def nombre_completo(self):
        return '{nombres} {a_paterno} {a_materno}'.format(
            nombres=self.nombres or '',
            a_paterno=self.apellido_paterno or '',
            a_materno=self.apellido_materno or ''
        )

    @property
    def edad(self):
        if not self.fecha_nacimiento:
            return ''
        edad1 = relativedelta(datetime.now().date(), self.fecha_nacimiento)
        if edad1.years < 1:
            if edad1.months <= 1:
                res = '{month} meses {day} días'.format(month=edad1.months, day=edad1.days)
            else:
                res = '{month} meses {day} días'.format(month=edad1.months, day=edad1.days)
        else:
            res = '{year} años, {month} meses {day} días'.format(year=edad1.years, month=edad1.months, day=edad1.days)

        return res

   # @classmethod
    #def crear_paciente(cls,tipo_documento, num_documento):


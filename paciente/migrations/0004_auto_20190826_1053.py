# Generated by Django 2.2.4 on 2019-08-26 15:53

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paciente', '0003_auto_20190825_2126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paciente',
            name='apellido_materno',
            field=models.CharField(blank=True, max_length=90, null=True, verbose_name='APELLIDO MATERNO'),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='apellido_paterno',
            field=models.CharField(blank=True, max_length=90, null=True, verbose_name='APELLIDO PATERNO'),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='fecha_nacimiento',
            field=models.DateField(null=True, verbose_name='FECHA DE NACIMIENTO'),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='nombres',
            field=models.CharField(blank=True, max_length=120, null=True, verbose_name='NOMBRES'),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='numero_documento',
            field=models.CharField(max_length=15, null=True, validators=[django.core.validators.MinLengthValidator(8)], verbose_name='NUMERO DE DOCUMENTO'),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='tipo_documento',
            field=models.CharField(blank=True, choices=[('1', 'DNI'), ('2', 'CARNET DE EXTRANJERIA'), ('3', 'INDOCUMENTADO')], default=1, max_length=1, null=True, verbose_name='TIPO DE DOCUMENTO'),
        ),
    ]

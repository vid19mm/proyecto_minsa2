from django.shortcuts import render
from django.views.generic import ListView, FormView, CreateView, UpdateView, DeleteView
from .models import Paciente
from .forms import BuscarPacienteForm, PacienteForm
from django.forms.forms import NON_FIELD_ERRORS
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse


# Create your views here.


class PacienteListView(ListView, FormView):
    template_name = 'listar_paciente.html'
    model = Paciente
    context_object_name = 'pacientes'
    form_class = BuscarPacienteForm
    form = BuscarPacienteForm

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        pacientes = []
        form.is_valid()
        encontro=0
        tipo_documento = form.cleaned_data.get('tipo_documento')
        numero_documento = form.cleaned_data.get('numero_documento')
        try:
            paciente = Paciente.objects.get(tipo_documento=tipo_documento, numero_documento=numero_documento)
            encontro=1
        except:
                paciente=''
        return render(request, 'listar_paciente.html', {'form': form, 'paciente': paciente, 'encontro':encontro})
        #return self.render_to_response(self.get_context_data().update({'form': form, 'paciente': paciente, 'encontro':encontro}))


class PacienteCreateView(CreateView):
    model = Paciente
    template_name = 'ingresar_paciente.html'
    form_class = PacienteForm
    form = PacienteForm
    success_url = reverse_lazy('listar_paciente')

   # def clean(self):
    #    cleaned_data = super(Paciente, self).clean()
     #   tipo_documento = cleaned_data.get('tipo_documento')
      #  numero_documento = cleaned_data.get('numero_documento')
       # paciente = Paciente.objects.get(tipo_documento=tipo_documento,numero_documento=numero_documento)
        #raise paciente.ValidationError(
         #       'La cantidad no es correcta'
          #  )


class PacienteUpdateView(UpdateView):
    template_name = 'ingresar_paciente.html'
    model = Paciente
    form_class = PacienteForm
    success_url = reverse_lazy('listar_paciente')


class PacienteDeleteView(DeleteView):
    template_name = 'eliminar_paciente.html'
    model = Paciente
    success_url = reverse_lazy('listar_paciente')

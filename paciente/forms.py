from django import forms
from paciente.constans import TIPDOC
from .models import Paciente


class BuscarPacienteForm(forms.Form):
    tipo_documento = forms.ChoiceField(label='TIPO DE DOCUMENTO', choices=TIPDOC, initial='1', required=False,
                                       widget=forms.Select(attrs={'class': 'form-control', 'style': 'height:32px;'}))
    numero_documento = forms.CharField(label='NUMERO DE DOCUMENTO', max_length=15, min_length=8,
                                       widget=forms.TextInput(attrs={
                                           'placeholder': 'Ingrese el numero de documento a buscar'}), required=False)


class PacienteForm(forms.ModelForm):
    edad = forms.CharField(label='EDAD', widget=forms.TextInput(attrs={'readonly': 'True', 'id': 'edad'}))
    class Meta:
        model = Paciente
        unique_together = ('tipo_documento', 'numero_documento')
        # fields ="__all__"
        fields = ('tipo_documento', 'numero_documento', 'nombres', 'apellido_paterno',
                  'apellido_materno', 'fecha_nacimiento')
        widgets = {
            'tipo_documento': forms.Select(attrs={'class': 'form-control', 'style': 'height:34px;'}),
            'numero_documento': forms.TextInput(attrs={'class': 'form-control'}),
            'nombres': forms.TextInput(attrs={'class': 'form-control'}),
            'apellido_paterno': forms.TextInput(attrs={'class': 'form-control'}),
            'apellido_materno': forms.TextInput(attrs={'class': 'form-control'}),
            'fecha_nacimiento': forms.DateInput(attrs={'placeholder': 'dd-MM-yyyy', 'class': 'form-control'}),

        }
